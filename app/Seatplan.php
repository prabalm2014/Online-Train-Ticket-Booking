<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seatplan extends Model
{
    protected $table = 'seatplan';
    protected $guarded = ['sid', 'updated_at', 'created_at'];
}
