<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addschedule extends Model
{
    protected $table = 'timeschedule';
    protected $guarded = ['updated_at', 'created_at'];
}
