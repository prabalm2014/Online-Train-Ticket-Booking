<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Users;
use App\Admin;
use App\Addschedule;
use App\Seatplan;
use App\Price;
use App\Ticket;

class Onlineticket extends Controller
{
    public function index()
    {
    	return view('welcome');
    }
    public function login(Request $request)
	{
		$users = new Users;
		$uname = $request->uname;
    	$pass = $request->password;
    	$user = Users::where('username', $uname)->where('password', $pass)->first();
    	$admin = Admin::where('username', $uname)->where('password', $pass)->first();
    	if($user)
    	{
    		session(['username'=> $uname]);
    		//$users = session('username');
    		//echo $users;
    		return redirect('user');
    	}
    	else if($admin) 
    	{
    		session(['username'=> $uname]);
    		//$admins = session('username');
    		//echo $admins;
    		return redirect('admin');
    	}
    	else
    	{
    		\Session::flash('flash_message', 'Username and Password Not Matched!');
    		return redirect('');
    	}
	}

	public function signup()
	{
		return view('usersignup');
	}

	public function usersignup(Request $request)
	{
		$users = new Users;
		$users->username  = $request->uname;
    	$users->email = $request->email;
    	$users->password = $request->password;
    	$users->phone = $request->phone;
    	$users->accountno = $request->accountno;

    	$usn = $request->uname;

    	if($cs = Users::pluck('username'))
    	{
    		foreach($cs as $i => $css)
	    	{
	    		if($cs[$i] == $usn)
		    	{
		    		\Session::flash('flash_message', 'Username already exist!');
					return redirect('signup');
		    	}
			} 
    	}
		if($acs = Admin::pluck('username'))
		{
			foreach ($acs as $j => $acss) 
			{
				if($acs[$j] == $usn)
				{
					\Session::flash('flash_message', 'Username already exist!');
					return redirect('signup');
				}
			}
		}
		$users->save();
		return redirect('/');
	}

	public function asignup()
	{
		return view('adminsignup');
	}
	public function adminsignup(Request $request)
	{
		$admin = new Admin;
		$admin->username  = $request->uname;
    	$admin->email = $request->email;
    	$admin->password = $request->password;
    	$admin->phone = $request->phone;
    	$admin->type = $request->type;
    	$admin->accountno = $request->accountno;
    	
    	$usn = $request->uname;

    	if($cs = Users::pluck('username'))
    	{
    		foreach($cs as $i => $css)
	    	{
	    		if($cs[$i] == $usn)
		    	{
		    		\Session::flash('flash_message', 'Username already exist!');
					return redirect('asignup');
		    	}
			} 
    	}
		if($acs = Admin::pluck('username'))
		{
			foreach ($acs as $j => $acss) 
			{
				if($acs[$j] == $usn)
				{
					\Session::flash('flash_message', 'Username already exist!');
					return redirect('asignup');
				}
			}
		}
		$admin->save();
		return redirect('/');
	}
	public function checkUser($uname)
	{
		$cs = Users::where('username', $uname)->first();
		$acs = Admin::where('username', $uname)->first();
		if($cs) 
		{
			echo "Username already exist, try another!";
		}
		else if($acs)
		{
			echo "Username already exist, try another!";
		}
	}

	public function adminpanel()
	{
		$admins = session('username');
		$cs = Ticket::all();
		if(!empty($admins))
		{
			return view('admin', compact('cs'));
		}
		else
		{
			return redirect('index');
		}
	}

	public function userpanel()
	{
		$users = session('username');
		$css = Ticket::where('name', $users)->get();
		if(!empty($users))
		{
			return view('user', compact('css'));
		}
		else
		{
			return redirect('index');
		}
	}

	public function addschedule()
	{
		return view('newschedule');
	}

	public function addscheduleConf(Request $request)
	{
		$addschedule = new Addschedule;
		$addschedule->train_no = $request->trainno;
		$addschedule->tname = $request->trainname;
		$addschedule->off_day = $request->offday;
		$addschedule->start_station = $request->startstation;
		$addschedule->start_time = $request->starttime;
		$addschedule->arrival_station = $request->arrivalstation;
		$addschedule->arrival_time = $request->arrivaltime;
		$addschedule->stop = $request->stop;
		if($addschedule->save())
		{
			\Session::flash('flash_message', 'New Time Schedule Added Successfuly!');
			return redirect('admin');
		}
	}

	public function addseat()
	{
		$cs = Addschedule::all();
		return view('seatadd', compact('cs'));
	}

	public function addseatConf(Request $request)
	{
		$seatplan = new Seatplan;
		$seatplan->train_no = $request->trainno;
		$seatplan->type  = $request->type;
		$seatplan->room_no = $request->roomno;
		$seatplan->date = $request->date;
		if($seatplan->save())
		{
			\Session::flash('flash_message', 'New Seat Plan Added Successfuly!');
			return redirect('admin');
		}
	}

	public function addprice()
	{
		$cs = Addschedule::all();
		return view('priceadd', compact('cs'));
	}

	public function addpriceconf(Request $request)
	{
		$price = new Price;
		$price->train_no = $request->trainno;
		$price->tname = $request->tname;
		$price->start_station = $request->startstation;
		$price->destination = $request->destination;
		$price->kilometer = $request->kilometer;
		$price->simple_2nd = $request->simple2nd;
		$price->mail_2nd = $request->mail2nd;
		$price->komiotar = $request->komiuter;
		$price->shulav = $request->shulav;
		$price->shuvon = $request->shuvon;
		$price->shuvon_chair = $request->shuvonchair;
		$price->chair_1st = $request->chair1st;
		$price->bath_1st  = $request->bath1st;
		$price->snigdha = $request->snigdha;
		$price->ac_chair = $request->acchair;
		$price->ac_bath = $request->acbath;
		if($price->save())
		{
			\Session::flash('flash_message', 'Price Added Successfuly!');
			return redirect('admin');
		}
	}

	public function bookticket()
	{
		$cs = Addschedule::paginate(10);
		return view('buyticket', compact('cs'));
	}
	public function ticketprice(Request $request)
	{
		$trainno = $request->trainno;
		session(['tno'=> $trainno]);
		$cs = Price::where('train_no', $trainno)->get();
		return view('ticketprice', compact('cs'));
	}

	public function seatplan(Request $request)//changed checkbox to radio
	{
		session()->put('all', $request->radio);
		return view('seatplan');
	}

	public function delseat()
	{
		$cs = Seatplan::all();
		return view('deleteseat', compact('cs'));
	}

	public function seatlist()
	{
		$tno = session('tno');
		
		//$css = Seatplan::all();
		$cs = Seatplan::Where('type', session('all'))
		->Where('train_no', $tno)
		->get();
		foreach ($cs as $value) 
		{
			echo "<div class='col-md-4' style='background-color: #ffe6ff;'>";
			echo "<table class='table' border='0'>";
			echo "<div  style='background-color: #ffffe6;'>";
			echo "<ul>";
				echo "<li>$value[train_no]</li>";
				echo "<li style='color:blue;'><b>$value[type]</b></li>";
				echo "<li><b>$value[room_no]</b></li>";
				echo "<li><b>$value[date]</b></li>";
			echo "</ul>";
			echo "<body>";
				echo "<tr align='center'>";
					echo "<td style=' box-sizing: border-box;background-color: white;'><b>Toilet</b></td>";
					echo "<td colspan='6'></td>";
					echo "<td style=' box-sizing: border-box;background-color: white;'><b>Toilet</b></td>";
				echo "</tr>";
				echo "<form method='post'>";
				echo "<tr align='center'>";
					if($value['s1'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S1 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S1 <input type='checkbox' name='$value[room_no]' value='s1' id='$value[date]'></td>";
					}
					if($value['s2'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S2 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S2 <input type='checkbox' name='$value[room_no]' value='s2' id='$value[date]'></td>";
					}
					if($value['s3'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S3 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S3 <input type='checkbox' name='$value[room_no]' value='s3' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s4'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S4 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S4 <input type='checkbox' name='$value[room_no]' value='s4' id='$value[date]'></td>";
					}
					if($value['s5'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S5 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S5 <input type='checkbox' name='$value[room_no]' value='s5' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s6'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S6 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S6 <input type='checkbox' name='$value[room_no]' value='s6' id='$value[date]'></td>";
					}
					if($value['s7'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S7 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S7 <input type='checkbox' name='$value[room_no]' value='s7' id='$value[date]'></td>";
					}
					if($value['s8'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S8 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S8 <input type='checkbox' name='$value[room_no]' value='s8' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s9'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S9 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S9 <input type='checkbox' name='$value[room_no]' value='s9' id='$value[date]'></td>";
					}
					if($value['s10'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S10 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S10 <input type='checkbox' name='$value[room_no]' value='s10' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s11'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S11 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S11 <input type='checkbox' name='$value[room_no]' value='s11' id='$value[date]'></td>";
					}
					if($value['s12'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S12 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S12 <input type='checkbox' name='$value[room_no]' value='s12' id='$value[date]'></td>";
					}
					if($value['s13'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S13 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S13 <input type='checkbox' name='$value[room_no]' value='s13' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s14'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S14 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S14 <input type='checkbox' name='$value[room_no]' value='s14' id='$value[date]'></td>";
					}
					if($value['s15'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S15 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S15 <input type='checkbox' name='$value[room_no]' value='s15' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s16'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S16 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S16 <input type='checkbox' name='$value[room_no]' value='s16' id='$value[date]'></td>";
					}
					if($value['s17'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S17 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S17 <input type='checkbox' name='$value[room_no]' value='s17' id='$value[date]'></td>";
					}
					if($value['s18'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S18 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S18 <input type='checkbox' name='$value[room_no]' value='s18' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s19'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S19 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S19 <input type='checkbox' name='$value[room_no]' value='s19' id='$value[date]'></td>";
					}
					if($value['s20'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S20 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S20 <input type='checkbox' name='$value[room_no]' value='s20' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s21'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S21 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S21 <input type='checkbox' name='$value[room_no]' value='s21' id='$value[date]'></td>";
					}
					if($value['s22'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S22 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S22 <input type='checkbox' name='$value[room_no]' value='s22' id='$value[date]'></td>";
					}
					if($value['s23'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S23 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S23 <input type='checkbox' name='$value[room_no]' value='s23' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s24'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S24 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S24 <input type='checkbox' name='$value[room_no]' value='s24' id='$value[date]'></td>";
					}
					if($value['s25'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S25 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S25 <input type='checkbox' name='$value[room_no]' value='s25' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s26'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S26 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S26 <input type='checkbox' name='$value[room_no]' value='s26' id='$value[date]'></td>";
					}
					if($value['s27'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S27 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S27 <input type='checkbox' name='$value[room_no]' value='s27' id='$value[date]'></td>";
					}
					if($value['s28'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S28 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S28 <input type='checkbox' name='$value[room_no]' value='s28' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s29'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S29 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S29 <input type='checkbox' name='$value[room_no]' value='s29' id='$value[date]'></td>";
					}
					if($value['s30'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S30 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S30 <input type='checkbox' name='$value[room_no]' value='s30' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s31'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S31 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S31 <input type='checkbox' name='$value[room_no]' value='s31' id='$value[date]'></td>";
					}
					if($value['s32'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S32 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S32 <input type='checkbox' name='$value[room_no]' value='s32' id='$value[date]'></td>";
					}
					if($value['s33'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S33 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S33 <input type='checkbox' name='$value[room_no]' value='s33' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s34'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S34 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S34 <input type='checkbox' name='$value[room_no]' value='s34' id='$value[date]'></td>";
					}
					if($value['s35'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S35 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S35 <input type='checkbox' name='$value[room_no]' value='s35' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s36'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S36 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S36 <input type='checkbox' name='$value[room_no]' value='s36' id='$value[date]'></td>";
					}
					if($value['s37'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S37 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S37 <input type='checkbox' name='$value[room_no]' value='s37' id='$value[date]'></td>";
					}
					if($value['s38'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S38 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S38 <input type='checkbox' name='$value[room_no]' value='s38' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s39'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S39 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S39 <input type='checkbox' name='$value[room_no]' value='s39' id='$value[date]'></td>";
					}
					if($value['s40'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S40 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S40 <input type='checkbox' name='$value[room_no]' value='s40' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s41'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S41 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S41 <input type='checkbox' name='$value[room_no]' value='s41' id='$value[date]'></td>";
					}
					if($value['s42'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S42 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S42 <input type='checkbox' name='$value[room_no]' value='s42' id='$value[date]'></td>";
					}
					if($value['s43'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S43 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S43 <input type='checkbox' name='$value[room_no]' value='s43' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s44'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S44 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S44 <input type='checkbox' name='$value[room_no]' value='s44' id='$value[date]'></td>";
					}
					if($value['s45'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S45 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S45 <input type='checkbox' name='$value[room_no]' value='s45' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "<tr align='center'>";
					if($value['s46'] == 1)
					{
						echo "<td style=' box-sizing: border-box; background-color: #752652;color:yellow;'>S46 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box; background-color: yellow;'>S46 <input type='checkbox' name='$value[room_no]' value='s46' id='$value[date]'></td>";
					}
					if($value['s47'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color:#752652;color:yellow;'>S47 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S47 <input type='checkbox' name='$value[room_no]' value='s47' id='$value[date]'></td>";
					}
					if($value['s48'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S48 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S48 <input type='checkbox' name='$value[room_no]' value='s48' id='$value[date]'></td>";
					}
					echo "<td></td><td></td><td></td>";
					if($value['s49'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S49 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: white;'>S49 <input type='checkbox' name='$value[room_no]' value='s49' id='$value[date]'></td>";
					}
					if($value['s50'] == 1)
					{
						echo "<td style=' box-sizing: border-box;background-color: #752652;color:yellow;'>S50 <input type='checkbox' name='simple_2nd' checked disabled></td>";
					}
					else{
						echo "<td style=' box-sizing: border-box;background-color: yellow;'>S50 <input type='checkbox' name='$value[room_no]' value='s50' id='$value[date]'></td>";
					}
				echo "</tr>";
				echo "</form>";
				echo "<tr align='center'>";
					echo "<td style=' box-sizing: border-box;background-color: white;'><b>Toilet</b></td>";
					echo "<td colspan='6'></td>";
					echo "<td style=' box-sizing: border-box;background-color: white;'><b>Toilet</b></td>";
				echo "</tr>";
				echo "</body>";
			echo "</div>";
			echo "</table>";
			echo "<div id='cnt'></div>";
			echo "</div>";
		}
	}
	public function data($sno,$rno,$dt)
	{
		$tno = session('tno');
		$str = Seatplan::where('train_no', $tno)
		->where('room_no',$rno)
		->update([$sno => 1]);
		//echo $tno.$sno.$rno.$dt;

		$cs = Addschedule::where('train_no', $tno)->first();
		$us = Users::where('username', session('username'))->first();

		$ticket = new Ticket;
		$ticket->name  = session('username');
		$ticket->train_no = $tno;
		$ticket->trainname = $cs['tname'];
		$ticket->roomno = $rno; 
		$ticket->seatno = $sno;
		$ticket->date = $dt;
		$ticket->start = $cs['start_station'];
		$ticket->dest = $cs['arrival_station'];
		$ticket->save();
	}

	//for users only

	public function showticketprice()
	{
		$cs = Price::paginate(20);
		return view('showprice', compact('cs'));
	}

	public function showschedule()
	{
		$cs = Addschedule::paginate(20);
		return view('showschedule', compact('cs'));
	}

	public function prints($name,$sno)
	{
		$cs = Ticket::where('name', $name)->where('seatno', $sno)->first();
		
		header("Content-Type: application/vnd.msword");
		header("Expire: 0");
		header("Cache-Control: must-revalidate, psot-check=0, pre-check=0");
		header("content-disposition: attachment;filename=$name.doc");

		echo "<br/><br/><br/><br/><br/>";
		echo "<div style='background-color:#FFFFB5;margin-left:400px;margin-right:400px;padding:2px;border:2px solid blue;'>";
		echo "<h1 align='center'>Bangladesh Railway</h1>";
		echo "<h2 align='center' style='color:blue;'>$cs[train_no] --- $cs[trainname]</h2>";
		echo "<h4 align='center'>Starting Station --- $cs[start]</h4>";
		echo "<h4 align='center'>Destination --- $cs[dest]</h4>";
		echo "<h4 align='center'>Room Number --- $cs[roomno]</h4>";
		echo "<h4 align='center'>Seat Number --- $cs[seatno]</h4>";
		echo "<h4 align='center'>Date --- $cs[date]</h4>";
		echo "<br/>";
		echo "</div>";
	}

	public function logout(Request $request)
	{
		$request->session()->flush();
		return redirect('');
	}
}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var x = $('input[name]');
	var chkId = '';
	$(':checkbox').click( function(){
		if (x.is(':checked')) {
			$("input[name]:checked").each ( function() {
				chkId = $(this).val() + ",";
		        chkId = chkId.slice(0, -1);
			});
			var v = $(this).val();
			var nm = $(this).attr("name");
			var dt = $(this).attr("id");
			//alert (dt);
			//console.log(v);

			$.ajax({
	    	url: "/data/"+ v +"/" + nm +"/" + dt, 
	    	type: "POST",
	    	success: function(result){$("#cnt").html(result);}
			});
		}
    });
});
</script>
