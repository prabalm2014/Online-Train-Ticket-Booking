<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'price';
    protected $guarded = ['pid', 'updated_at', 'created_at'];
}
