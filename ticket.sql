-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2016 at 11:56 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `aid` int(15) NOT NULL,
  `accountno` varchar(25) NOT NULL,
  `balance` int(30) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`aid`, `accountno`, `balance`, `updated_at`, `created_at`) VALUES
(1, '101.120.6809', 400000, '2016-10-24 07:57:18', '2016-10-24 07:57:18'),
(2, '101.120.6810', 250000, '2016-10-24 07:57:18', '2016-10-24 07:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adid` int(15) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` varchar(20) NOT NULL,
  `accountno` varchar(25) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adid`, `username`, `email`, `password`, `phone`, `type`, `accountno`, `updated_at`, `created_at`) VALUES
(1, 'pro', 'pro', 'pro', '090909', 'admin', '101.120.6809', '2016-10-24 08:08:54', '2016-10-24 08:08:54'),
(2, 'prabal', 'pm@gmail.com', '123456', '01673838901', 'super', '101.120.6809', '2016-10-24 04:12:45', '2016-10-24 04:12:45'),
(3, 'malek', 'pro@gmail.com', '123456', '01673838901', 'super', '101.120.6810', '2016-10-24 04:13:33', '2016-10-24 04:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `bookdate`
--

CREATE TABLE `bookdate` (
  `did` int(15) NOT NULL,
  `sid` int(15) NOT NULL,
  `train_no` int(15) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `pid` int(15) NOT NULL,
  `train_no` int(200) NOT NULL,
  `tname` varchar(255) NOT NULL,
  `start_station` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `kilometer` int(200) NOT NULL,
  `simple_2nd` int(200) DEFAULT NULL,
  `mail_2nd` int(200) DEFAULT NULL,
  `komiotar` int(200) DEFAULT NULL,
  `shulav` int(200) DEFAULT NULL,
  `shuvon` int(200) DEFAULT NULL,
  `shuvon_chair` int(200) DEFAULT NULL,
  `chair_1st` int(200) DEFAULT NULL,
  `bath_1st` int(200) DEFAULT NULL,
  `snigdha` int(200) DEFAULT NULL,
  `ac_chair` int(200) DEFAULT NULL,
  `ac_bath` int(200) DEFAULT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`pid`, `train_no`, `tname`, `start_station`, `destination`, `kilometer`, `simple_2nd`, `mail_2nd`, `komiotar`, `shulav`, `shuvon`, `shuvon_chair`, `chair_1st`, `bath_1st`, `snigdha`, `ac_chair`, `ac_bath`, `updated_at`, `created_at`) VALUES
(1, 701, 'Subarna Express Train', 'Dhaka Kamalapur Railway Station', 'Chittagong, Bangladesh', 364, 90, 115, 145, 175, 285, 345, 460, 685, 656, 788, 1179, '2016-10-25 07:43:52', '2016-10-25 07:43:52'),
(2, 701, 'Subarna Express Train', 'Dhaka Kamalapur', 'Chittagong, Bangladesh', 390, 280, 285, 400, 450, 480, 480, 560, 800, 900, 950, 1200, '2016-10-26 08:31:17', '2016-10-26 08:31:17'),
(3, 702, 'Ekota Express Train', 'Chittagong, Bangladesh', 'Dhaka', 410, 90, 120, 150, 200, 320, 380, 450, 520, 620, 780, 900, '2016-10-26 21:36:21', '2016-10-26 21:36:21');

-- --------------------------------------------------------

--
-- Table structure for table `seatplan`
--

CREATE TABLE `seatplan` (
  `sid` int(15) NOT NULL,
  `train_no` int(15) NOT NULL,
  `type` varchar(50) NOT NULL,
  `room_no` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `s1` int(1) NOT NULL DEFAULT '0',
  `s2` int(1) NOT NULL DEFAULT '0',
  `s3` int(1) NOT NULL DEFAULT '0',
  `s4` int(1) NOT NULL DEFAULT '0',
  `s5` int(1) NOT NULL DEFAULT '0',
  `s6` int(1) NOT NULL DEFAULT '0',
  `s7` int(1) NOT NULL DEFAULT '0',
  `s8` int(1) NOT NULL DEFAULT '0',
  `s9` int(1) NOT NULL DEFAULT '0',
  `s10` int(1) NOT NULL DEFAULT '0',
  `s11` int(1) NOT NULL DEFAULT '0',
  `s12` int(1) NOT NULL DEFAULT '0',
  `s13` int(1) NOT NULL DEFAULT '0',
  `s14` int(1) NOT NULL DEFAULT '0',
  `s15` int(1) NOT NULL DEFAULT '0',
  `s16` int(1) NOT NULL DEFAULT '0',
  `s17` int(1) NOT NULL DEFAULT '0',
  `s18` int(1) NOT NULL DEFAULT '0',
  `s19` int(1) NOT NULL DEFAULT '0',
  `s20` int(1) NOT NULL DEFAULT '0',
  `s21` int(1) NOT NULL DEFAULT '0',
  `s22` int(1) NOT NULL DEFAULT '0',
  `s23` int(1) NOT NULL DEFAULT '0',
  `s24` int(1) NOT NULL DEFAULT '0',
  `s25` int(1) NOT NULL DEFAULT '0',
  `s26` int(1) NOT NULL DEFAULT '0',
  `s27` int(1) NOT NULL DEFAULT '0',
  `s28` int(1) NOT NULL DEFAULT '0',
  `s29` int(1) NOT NULL DEFAULT '0',
  `s30` int(1) NOT NULL DEFAULT '0',
  `s31` int(1) NOT NULL DEFAULT '0',
  `s32` int(1) NOT NULL DEFAULT '0',
  `s33` int(1) NOT NULL DEFAULT '0',
  `s34` int(1) NOT NULL DEFAULT '0',
  `s35` int(1) NOT NULL DEFAULT '0',
  `s36` int(1) NOT NULL DEFAULT '0',
  `s37` int(1) NOT NULL DEFAULT '0',
  `s38` int(1) NOT NULL DEFAULT '0',
  `s39` int(1) NOT NULL DEFAULT '0',
  `s40` int(1) NOT NULL DEFAULT '0',
  `s41` int(1) NOT NULL DEFAULT '0',
  `s42` int(1) NOT NULL DEFAULT '0',
  `s43` int(1) NOT NULL DEFAULT '0',
  `s44` int(1) NOT NULL DEFAULT '0',
  `s45` int(1) NOT NULL DEFAULT '0',
  `s46` int(1) NOT NULL DEFAULT '0',
  `s47` int(1) NOT NULL DEFAULT '0',
  `s48` int(1) NOT NULL DEFAULT '0',
  `s49` int(1) NOT NULL DEFAULT '0',
  `s50` int(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seatplan`
--

INSERT INTO `seatplan` (`sid`, `train_no`, `type`, `room_no`, `date`, `s1`, `s2`, `s3`, `s4`, `s5`, `s6`, `s7`, `s8`, `s9`, `s10`, `s11`, `s12`, `s13`, `s14`, `s15`, `s16`, `s17`, `s18`, `s19`, `s20`, `s21`, `s22`, `s23`, `s24`, `s25`, `s26`, `s27`, `s28`, `s29`, `s30`, `s31`, `s32`, `s33`, `s34`, `s35`, `s36`, `s37`, `s38`, `s39`, `s40`, `s41`, `s42`, `s43`, `s44`, `s45`, `s46`, `s47`, `s48`, `s49`, `s50`, `updated_at`, `created_at`) VALUES
(1, 701, 'Simple 2nd Class', 'A', '2016-10-31', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-10-30 05:23:04', '0000-00-00 00:00:00'),
(2, 701, 'Simple 2nd Class', 'B', '2016-10-31', 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-10-29 09:30:57', '0000-00-00 00:00:00'),
(3, 701, 'Bath AC', 'H', '2016-10-31', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-10-30 01:06:47', '0000-00-00 00:00:00'),
(4, 701, 'Bath AC', 'H', '2016-10-31', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-10-30 01:06:47', '0000-00-00 00:00:00'),
(5, 701, 'AC Chair', 'J', '2016-10-31', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-10-30 05:24:04', '2016-10-25 04:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `tkid` int(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `train_no` int(15) NOT NULL,
  `trainname` varchar(255) NOT NULL,
  `roomno` varchar(20) NOT NULL,
  `seatno` varchar(20) NOT NULL,
  `date` varchar(30) NOT NULL,
  `start` varchar(255) NOT NULL,
  `dest` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`tkid`, `name`, `train_no`, `trainname`, `roomno`, `seatno`, `date`, `start`, `dest`, `updated_at`, `created_at`) VALUES
(1, 'malek', 701, 'Subarna Express Train', 'A', 's1', '2016-10-31', 'Chittagong, Bangladesh', 'Dhaka Kamalapur Railway Station', '2016-10-30 00:36:17', '2016-10-30 00:36:17'),
(2, 'malek', 701, 'Subarna Express Train', 'H', 's3', '2016-10-31', 'Chittagong, Bangladesh', 'Dhaka Kamalapur Railway Station', '2016-10-30 01:06:47', '2016-10-30 01:06:47'),
(3, 'prabalm', 701, 'Subarna Express Train', 'J', 's2', '2016-10-31', 'Chittagong, Bangladesh', 'Dhaka Kamalapur Railway Station', '2016-10-30 02:42:00', '2016-10-30 02:42:00'),
(4, 'malek', 701, 'Subarna Express Train', 'A', 's2', '2016-10-31', 'Chittagong, Bangladesh', 'Dhaka Kamalapur Railway Station', '2016-10-30 05:23:04', '2016-10-30 05:23:04'),
(5, 'prabalm', 701, 'Subarna Express Train', 'J', 's1', '2016-10-31', 'Chittagong, Bangladesh', 'Dhaka Kamalapur Railway Station', '2016-10-30 05:24:04', '2016-10-30 05:24:04');

-- --------------------------------------------------------

--
-- Table structure for table `timeschedule`
--

CREATE TABLE `timeschedule` (
  `train_no` int(15) NOT NULL,
  `tname` varchar(255) NOT NULL,
  `off_day` varchar(200) NOT NULL,
  `start_station` varchar(255) NOT NULL,
  `start_time` time NOT NULL,
  `arrival_station` varchar(255) NOT NULL,
  `arrival_time` time NOT NULL,
  `stop` varchar(1000) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeschedule`
--

INSERT INTO `timeschedule` (`train_no`, `tname`, `off_day`, `start_station`, `start_time`, `arrival_station`, `arrival_time`, `stop`, `updated_at`, `created_at`) VALUES
(701, 'Subarna Express Train', 'Friday', 'Chittagong, Bangladesh', '07:00:00', 'Dhaka Kamalapur Railway Station', '12:40:00', 'Direct', '2016-10-25 01:07:58', '2016-10-25 01:07:58'),
(702, 'Subarna Express Train', 'Friday', 'Dhaka', '03:00:00', 'Chittagong, Bangladesh', '08:30:00', 'Direct', '2016-10-25 01:20:51', '2016-10-25 01:20:51'),
(703, 'Mohanagar Goduli', 'No', 'Chittagong, Bangladesh', '03:00:00', 'Dhaka Kamalapur Railway Station', '10:15:00', 'Dont Know', '2016-10-25 01:23:41', '2016-10-25 01:23:41'),
(704, 'Mohanagar Provati', 'No', 'Dhaka Kamalapur', '07:40:00', 'Chittagong, Bangladesh', '02:55:00', 'Dont Know', '2016-10-25 01:24:54', '2016-10-25 01:24:54'),
(705, 'Ekota Express Train', 'Tuesday', 'Dhaka Kamalapur', '10:00:00', 'Dinajpur, Bangladesh', '07:45:00', 'Dont Know', '2016-10-25 01:26:23', '2016-10-25 01:26:23'),
(706, 'Ekota Express Train', 'Monday', 'Dinajpur, Bangladesh', '09:20:00', 'Dhaka Kamalapur Railway Station', '07:10:00', 'Dont Know', '2016-10-25 01:35:44', '2016-10-25 01:35:44');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(15) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'user',
  `accountno` varchar(25) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `phone`, `type`, `accountno`, `updated_at`, `created_at`) VALUES
(4, 'pm', 'pm@gmail.com', 'sdsd', 'sdsd', 'user', 'sdsdd', '2016-10-24 05:30:52', '2016-10-24 05:30:52'),
(12, 'prabalm', 'pm@gmail.com', '123456', 'jkhj', 'user', 'khjkh', '2016-10-24 10:08:08', '2016-10-24 10:08:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adid`);

--
-- Indexes for table `bookdate`
--
ALTER TABLE `bookdate`
  ADD PRIMARY KEY (`did`),
  ADD KEY `sid` (`sid`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `train_no` (`train_no`);

--
-- Indexes for table `seatplan`
--
ALTER TABLE `seatplan`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `train_no` (`train_no`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`tkid`);

--
-- Indexes for table `timeschedule`
--
ALTER TABLE `timeschedule`
  ADD PRIMARY KEY (`train_no`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `aid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bookdate`
--
ALTER TABLE `bookdate`
  MODIFY `did` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
  MODIFY `pid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `seatplan`
--
ALTER TABLE `seatplan`
  MODIFY `sid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `tkid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookdate`
--
ALTER TABLE `bookdate`
  ADD CONSTRAINT `bookdate_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `seatplan` (`sid`);

--
-- Constraints for table `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `price_ibfk_1` FOREIGN KEY (`train_no`) REFERENCES `timeschedule` (`train_no`);

--
-- Constraints for table `seatplan`
--
ALTER TABLE `seatplan`
  ADD CONSTRAINT `seatplan_ibfk_1` FOREIGN KEY (`train_no`) REFERENCES `timeschedule` (`train_no`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
