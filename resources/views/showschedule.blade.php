<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Show Schedule</title>
</head>
<body>
	<div class="container">
        <div class="row">
            <br/>
            <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                <div class="panel-body">
                    <h1 align="center">Show Schedule</h1>
                      <table class="table">
						<thead>
							<tr>
								<th>Train Number</th>
								<th>Train Name</th>
								<th>Weekly Off-Day</th>
								<th>Starting Station</th>
								<th>Starting Time</th>
								<th>Arrival Station</th>
								<th>Arrival Time</th>
								<th>Stop</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cs as $value)
								<tr>
			                    	<td>{{$value['train_no']}}</td>
			                    	<td>{{$value['tname']}}</td>
			                    	<td>{{$value['off_day']}}</td>
			                    	<td>{{$value['start_station']}}</td>
			                    	<td>{{$value['start_time']}}</td>
			                    	<td>{{$value['arrival_station']}}</td>
			                    	<td>{{$value['arrival_time']}}</td>
			                    	<td>{{$value['stop']}}</td>
			                    </tr>
			                @endforeach
						</tbody>
					</table>
					<div style="text-align: center;">
						<p align="center">{{$cs->links()}}</p>
						<a class="btn btn-success" href="user">Home</a>
					</div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>