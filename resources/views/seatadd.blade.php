<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Seat Plan</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
        $( "#datepicker" ).datepicker({dateFormat:"yy-mm-dd"});
        } );
    </script>
</head>
<body>
	<div class="container">
        <div class="row">
            <br/>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                    <div class="panel-body">
                        <h1 align="center">Add Seat Plan</h1>

                        <form action="seatconfirm" method="POST">
                            
                            <div class="form-group">
                                <select class="form-control" name="trainno" required autofocus>
                                    <option disabled selected>Train Number...</option>
                                    @foreach ($cs as $value) 
                                        <option value="{{$value['train_no']}}">{{$value['train_no']}} ({{$value['tname']}})</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="type" required>
                                    <option disabled selected>Type...</option>
                                    <option value="Simple 2nd Class">Simple 2nd Class</option>
                                    <option value="Mail 2nd Class">Mail 2nd Class</option>
                                    <option value="Komutar">Komutar</option>
                                    <option value="Shulav">Shulav</option>
                                    <option value="Shuvon">Shuvon</option>
                                    <option value="Shuvon Chair">Shuvon Chair</option>
                                    <option value="Chair 1st Class">Chair 1st Class</option>
                                    <option value="Bath 1st Class">Bath 1st Class</option>
                                    <option value="Snigdha">Snigdha</option>
                                    <option value="AC Chair">AC Chair</option>
                                    <option value="Bath AC">Bath AC</option>
                                </select>
                            </div>
                        
                            <div class="form-group">
                                <select class="form-control" name="roomno" required>
                                    <option disabled selected>Room No...</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                    <option value="E">E</option>
                                    <option value="F">F</option>
                                    <option value="G">G</option>
                                    <option value="H">H</option>
                                    <option value="I">I</option>
                                    <option value="J">J</option>
                                    <option value="K">K</option>
                                    <option value="L">L</option>
                                    <option value="M">M</option>
                                    <option value="N">N</option>
                                    <option value="O">O</option>
                                    <option value="P">P</option>
                                    <option value="Q">Q</option>
                                    <option value="R">R</option>
                                    <option value="S">S</option>
                                    <option value="T">T</option>
                                    <option value="U">U</option>
                                    <option value="V">V</option>
                                    <option value="W">W</option>
                                    <option value="X">X</option>
                                    <option value="Y">Y</option>
                                    <option value="Z">Z</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="text" name="date" id="datepicker" class="form-control" required placeholder="Date (yyyy-mm-dd)">
                            </div>

                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="submit" name="submit" value="Add Plan" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>