<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Seat Plan</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function plan()
    {
		setInterval(function showUser()
		{
		$.ajax({
	    	url: "/seatlist", 
	    	type: "GET",
	    	success: function(result)
	    	{
	    		$("#cnt").html(result);
	    	}
		});
		}, 1000);
    }
    </script>
</head>
<body style="background-color: lavenderblush;" onload="plan()">
	<div class="container-fluid">
        <div class="row">
            <br/>
            <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
	            <div class="panel-body">
	            	<h1 align="center">Seat Plan</h1>
	            	<a href="../../">Buy Ticket /</a><a href="../../../ticketprice/{{session('tno')}}"> Ticket Price / </a><a href="/bookticket/ticketprice/{{session('tno')}}/seatplan"> Seat Plan / </a><br/><br/>
	            	<div class="col-sm-6">
	            		<span style="padding: 10px;background-color: yellow;">Window Seat</span>
	            		<span style="padding: 10px;background-color: black;color: white;">Booked</span>
	            		<span style="padding: 10px;border: 1px solid blue;">Empty</span>
	            		<span style="padding: 10px;background-color: red;color: white;border-radius: 4px;"><strong>Warning!</strong> Once You Click On Seat, You Can't Change It</span>
	            	</div>
	            </div>
                <div class="panel-body">
                	<span id="cnt"><img src="loading.gif"></span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>