<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ticket Price</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: lavenderblush;">
	<div class="container-fluid">
        <div class="row">
            <br/>
            <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                <div class="panel-body">
                	<a href="../">Buy Ticket /</a><a href="../ticketprice/{{session('tno')}}"> Ticket Price / </a>
                    <h1 align="center">Ticket Price</h1>
                      <table class="table">
						<thead>
							<tr>
								<th>Train Number</th>
								<th>Train Name</th>
								<th>Starting Station</th>
								<th>Destination</th>
								<th>Kilometer</th>
								<th>Simple 2nd Class</th>
								<th>Mail 2nd Class</th>
								<th>KomioTur</th>
								<th>Shulav</th>
								<th>Shuvon</th>
								<th>Shuvon Chair</th>
								<th>Chair 1st Class</th>
								<th>Bath 1st Class</th>
								<th>Snigdha</th>
								<th>AC Chair</th>
								<th>AC Bath</th>
								<th>Seat Plan</th>
							</tr>
						</thead>
						<tbody>
							<form action="/bookticket/ticketprice/{{session('tno')}}/seatplan/" method="post">
							@foreach($cs as $value)
								<tr>
			                    	<td>{{$value['train_no']}}</td>
			                    	<td>{{$value['tname']}}</td>
			                    	<td>{{$value['start_station']}}</td>
			                    	<td>{{$value['destination']}}</td>
			                    	<td>{{$value['kilometer']}}</td>
			                    	<td><input type="radio" name="radio" value="Simple 2nd Class"> {{$value['simple_2nd']}}</td>
			                    	<td><input type="radio" name="radio" value="Mail 2nd Class"> {{$value['mail_2nd']}}</td>
			                    	<td><input type="radio" name="radio" value="Komutar"> {{$value['komiotar']}}</td>
			                    	<td><input type="radio" name="radio" value="Shulav"> {{$value['shulav']}}</td>
			                    	<td><input type="radio" name="radio" value="Shuvon"> {{$value['shuvon']}}</td>
			                    	<td><input type="radio" name="radio" value="Shuvon Chair"> {{$value['shuvon_chair']}}</td>
			                    	<td><input type="radio" name="radio" value="Chair 1st Class"> {{$value['chair_1st']}}</td>
			                    	<td><input type="radio" name="radio" value="Bath 1st Class"> {{$value['bath_1st']}}</td>
			                    	<td><input type="radio" name="radio" value="Snigdha"> {{$value['snigdha']}}</td>
			                    	<td><input type="radio" name="radio" value="AC Chair"> {{$value['ac_chair']}}</td>
			                    	<td><input type="radio" name="radio" value="Bath AC"> {{$value['ac_bath']}}</td>
			                    	<td><!-- <a class="btn btn-success" href="/bookticket/ticketprice/{{session('tno')}}/seatplan">Seat Plan</a> -->
			                    	<input type="hidden" name="_token" value="{{csrf_token()}}">
			                    		<input type="submit" name="submit" class="btn btn-success" value="Seat Plan">
			                    	</td>
			                    </tr>
			                @endforeach
			                </form>
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>