<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Delete Seat Plan</title>
</head>
<body>
	<div class="container">
        <div class="row">
            <br/>
            <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                <div class="panel-body">
                    <h1 align="center">Delete Seat Plan</h1>
                      <table class="table">
						<thead>
							<tr>
								<th>Train Number</th>
								<th>Type</th>
								<th>Room Number</th>
								<th>Date</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cs as $value)
								<tr>
			                    	<td>{{$value['train_no']}}</td>
			                    	<td>{{$value['type']}}</td>
			                    	<td>{{$value['room_no']}}</td>
			                    	<td>{{$value['date']}}</td>
			                    	<td><a href="" class="btn btn-danger">Delete</a></td>
			                    </tr>
			                @endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>