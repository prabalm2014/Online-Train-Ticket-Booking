<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Ticket Price</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                <div class="panel-body">
                    <h1 align="center">Ticket Price</h1>
                      <table class="table">
						<thead>
							<tr>
								<th>Train Number</th>
								<th>Train Name</th>
								<th>Starting Station</th>
								<th>Destination</th>
								<th>Kilometer</th>
								<th>Simple 2nd Class</th>
								<th>Mail 2nd Class</th>
								<th>KomioTur</th>
								<th>Shulav</th>
								<th>Shuvon</th>
								<th>Shuvon Chair</th>
								<th>Chair 1st Class</th>
								<th>Bath 1st Class</th>
								<th>Snigdha</th>
								<th>AC Chair</th>
								<th>AC Bath</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cs as $value)
								<tr>
			                    	<td>{{$value['train_no']}}</td>
			                    	<td>{{$value['tname']}}</td>
			                    	<td>{{$value['start_station']}}</td>
			                    	<td>{{$value['destination']}}</td>
			                    	<td>{{$value['kilometer']}}</td>
			                    	<td>{{$value['simple_2nd']}}</td>
			                    	<td>{{$value['mail_2nd']}}</td>
			                    	<td>{{$value['komiotar']}}</td>
			                    	<td>{{$value['shulav']}}</td>
			                    	<td>{{$value['shuvon']}}</td>
			                    	<td>{{$value['shuvon_chair']}}</td>
			                    	<td>{{$value['chair_1st']}}</td>
			                    	<td>{{$value['bath_1st']}}</td>
			                    	<td>{{$value['snigdha']}}</td>
			                    	<td>{{$value['ac_chair']}}</td>
			                    	<td>{{$value['ac_bath']}}</td>
			                    </tr>
			                @endforeach
						</tbody>
					</table>
					<div style="text-align: center;">
						<p align="center">{{$cs->links()}}</p>
						<a class="btn btn-success" href="user">Home</a>
					</div>
                </div>
            </div>			
		</div>
	</div>
</body>
</html>