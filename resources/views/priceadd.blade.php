<!DOCTYPE html>
<html>
<head>
	<title>Add Ticket Price</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add New Schedule</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
</head>
<body>
	<div class="container">
        <div class="row">
            <br/>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                    <div class="panel-body">
                        <h1 align="center">Add Ticket Price</h1>

                        <form action="addpriceconf" method="POST">
                            <div class="form-group">
                                <select class="form-control" name="trainno" required autofocus>
                                    <option disabled selected>Train Number...</option>
                                    @foreach ($cs as $value) 
                                        <option value="{{$value['train_no']}}">{{$value['train_no']}}</option>
                                    @endforeach
                                </select>
                            </div>
                           
                            <div class="form-group">
                                <select class="form-control" name="tname" required autofocus>
                                    <option disabled selected>Train Name...</option>
                                    @foreach ($cs as $value)
                                        <option value="{{$value['tname']}}">{{$value['tname']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="text" name="startstation" class="form-control" required placeholder="Starting Station">
                            </div>

                            <div class="form-group">
                                <input type="text" name="destination" class="form-control" required placeholder="Destination">
                            </div>

                            <div class="form-group">
                                <input type="text" name="kilometer" class="form-control" required placeholder="Kilometer">
                            </div>

                            <div class="form-group">
                                <input type="text" name="simple2nd" class="form-control" required placeholder="Simple 2nd Class">
                            </div>

                            <div class="form-group">
                                <input type="text" name="mail2nd" class="form-control" required placeholder="Mail 2nd Class">
                            </div>

                            <div class="form-group">
                                <input type="text" name="komiuter" class="form-control" required placeholder="Komiuter">
                            </div>

                            <div class="form-group">
                                <input type="text" name="shulav" class="form-control" required placeholder="Shulav">
                            </div>

                            <div class="form-group">
                                <input type="text" name="shuvon" class="form-control" required placeholder="Shuvon">
                            </div>

                            <div class="form-group">
                                <input type="text" name="shuvonchair" class="form-control" required placeholder="Shuvon Chair">
                            </div>

                            <div class="form-group">
                                <input type="text" name="chair1st" class="form-control" required placeholder="Chair 1st Class">
                            </div>

                            <div class="form-group">
                                <input type="text" name="bath1st" class="form-control" required placeholder="Bath 1st Class">
                            </div>

                            <div class="form-group">
                                <input type="text" name="snigdha" class="form-control" required placeholder="Snigdha">
                            </div>

                            <div class="form-group">
                                <input type="text" name="acchair" class="form-control" required placeholder="AC Chair">
                            </div>

                            <div class="form-group">
                                <input type="text" name="acbath" class="form-control" required placeholder="AC Bath">
                            </div>

                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="submit" name="submit" value="Add Price" class="btn btn-primary">
                            <h5 align="center"><a href="/admin">Back</a></h5>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>