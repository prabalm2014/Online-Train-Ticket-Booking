<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Admin Panel</title>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand">OTB</span>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="admin">Home</a></li>
                    <li><a href="addschedule">Add Schedule</a></li>
                    <li><a href="addprice">Add Ticket Price</a></li>
                    <li><a href="addseat">Add Seat Plan</a></li>
                    <li><a href="delseat">Delete Seat Plan</a></li>
                    <li><a href="#">Post Notice</a></li>
                    <li><a href="asignup">Add Admin</a></li>
                   
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">List<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Price</a></li>
                            <li><a href="#">Time Schedule</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="bookticket"><span class="glyphicon glyphicon-shopping-cart"></span> Buy Ticket</a></li>
                    <li><a href="logout"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        @if(Session::has('flash_message'))
            <div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>{{Session::get('flash_message')}}</div>
        @endif
    </div>
    <div class="container">         
        <table class="table">
            <thead>
                <tr>
                    <th>Train No</th>
                    <th>Train Name</th>
                    <th>Room Number</th>
                    <th>Seat Number</th>
                    <th>Reservation Date</th>
                    <th>Starting Station</th>
                    <th>Destination</th>
                    <th>Print</th>
                </tr>
            </thead>
        <tbody>
            @foreach($cs as $value)
            <tr class="success">
                <td>{{$value['train_no']}}</td>
                <td>{{$value['trainname']}}</td>
                <td>{{$value['roomno']}}</td>
                <td>{{$value['seatno']}}</td>
                <td>{{$value['date']}}</td>
                <td>{{$value['start']}}</td>
                <td>{{$value['dest']}}</td>
                <td><a href="print/{{$value['name']}}/{{$value['seatno']}}" class="btn btn-success">Print</a></td>
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</body>
</html>