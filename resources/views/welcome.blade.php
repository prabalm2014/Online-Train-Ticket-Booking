<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Online Ticket Booking(OTB) | Bangladesh Railway</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style type="text/css">
            body{
                background-color: #FAFFFF;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>OTB</h1>
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-primary" style="background-color: white;">
                        <div class="panel-body">
                            <h3 style="color: blue;">Notice</h3><hr/>
                            <h4>Notice will display here</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-7" style="position: fixed;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 style="color: blue;">Login</h3>
                            
                            <form action="login" method="POST">
                                <div class="form-group">
                                    <label for="username">Username:</label>
                                    <input type="text" name="uname" class="form-control" required placeholder="Username" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" name="password" class="form-control" required placeholder="Password">
                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                @if(Session::has('flash_message'))
                                    <div class='alert alert-success alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>{{Session::get('flash_message')}}</div>
                                @endif
                                <input type="submit" name="submit" value="Login" class="btn btn-primary">
                                <h5 align="center"><a href="signup">Sign Up Here</a></h5>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
