<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Sign Up</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style type="text/css">
            body{
                background-color: #FAFFFF;
            }
        </style>
        <script type="text/javascript">
            function checkusers()
            {
                var x = document.getElementById("uname").value;
                $.ajax({
                    url: "checkuser/"+ x, 
                    type: "GET",
                    success: function(result)
                    {
                        $("#cnt").html(result);
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <br/>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                        <div class="panel-body">
                            <h1 align="center">Sign Up</h1>
                            <form action="adminsignup" method="POST">
                                <div class="form-group">
                                    <input type="text" id="uname" name="uname" class="form-control" required placeholder="Username" autofocus onblur="checkusers()">
                                </div>
                                <span id="cnt" style="color: red;"></span>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" required placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" required placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control" required placeholder="Phone Number">
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="type" required>
                                    	<option disabled selected>Admin Type...</option>
                                    	<option value="super">Super Admin</option>
                                    	<option value="admin">Admin</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="accountno" class="form-control" required placeholder="Account Number">
                                </div>
                                @if(Session::has('flash_message'))
                                    <span style="color: red;">{{Session::get('flash_message')}}</span><br/>
                                @endif
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="submit" name="submit" value="Sign Up" class="btn btn-primary">
                                <h5 align="center"><a href="admin">Back</a></h5>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
