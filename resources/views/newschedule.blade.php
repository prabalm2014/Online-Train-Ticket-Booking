<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add New Schedule</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <script src="//code.jquery.com/jquery-1.12.1.min.js"></script>
    <script src="timepicker.js"></script>
    <script>
        $( function() {
        $( "#datepicker" ).timepicker({timeFormat: 'h:mm',scrollbar: true});
        } );

        $( function() {
        $( "#datepicker2" ).timepicker({timeFormat: 'h:mm',scrollbar: true});
        } );
    </script>
</head>
<body>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
	<div class="container">
        <div class="row">
            <br/>
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-primary" style="box-shadow: 1px 4px 4px 4px #CCCCEB;">
                    <div class="panel-body">
                        <h1 align="center">Add New Schedule</h1>

                        <form action="addconfirm" method="POST">
                            <div class="form-group">
                                <input type="text" name="trainno" class="form-control" required placeholder="Train Number" autofocus>
                            </div>
                           
                            <div class="form-group">
                                <input type="text" name="trainname" class="form-control" required placeholder="Train Name">
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="offday" required autofocus>
                                    <option disabled selected>Weekly Off-Day...</option>
                                    <option value="Sunday">Sunday</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="text" name="startstation" class="form-control" required placeholder="Start Station">
                            </div>

                            <div class="form-group">
                                <input type="text" name="starttime" id="datepicker" class="form-control" required placeholder="Start Time (h:m:s)">
                            </div>

                            <div class="form-group">
                                <input type="text" name="arrivalstation" class="form-control" required placeholder="Arrival Station">
                            </div>

                            <div class="form-group">
                                <input type="text" name="arrivaltime" id="datepicker2" class="form-control" required placeholder="Arrival Time (h:m:s)">
                            </div>

                            <div class="form-group">
                                <textarea name="stop" rows="0" cols="30" class="form-control" required placeholder="Stop..."></textarea>
                            </div>

                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="submit" name="submit" value="Add Schedule" class="btn btn-primary">
                            <h5 align="center"><a href="admin">Back</a></h5>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>