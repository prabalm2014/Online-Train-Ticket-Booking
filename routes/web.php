<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('', 'Onlineticket@index');
Route::post('login', 'Onlineticket@login');
Route::get('signup', 'Onlineticket@signup');
Route::post('usersignup', 'Onlineticket@usersignup');
Route::get('asignup', 'Onlineticket@asignup');
Route::post('adminsignup', 'Onlineticket@adminsignup');
Route::get('checkuser/{uname}', 'Onlineticket@checkUser');
Route::get('admin', 'Onlineticket@adminpanel');
Route::get('user', 'Onlineticket@userpanel');
Route::get('addschedule', 'Onlineticket@addschedule');
Route::post('addconfirm', 'Onlineticket@addscheduleConf');
Route::get('addseat', 'Onlineticket@addseat');
Route::post('seatconfirm', 'Onlineticket@addseatConf');
Route::get('addprice', 'Onlineticket@addprice');
Route::post('addpriceconf', 'Onlineticket@addpriceconf');
Route::get('bookticket', 'Onlineticket@bookticket');
Route::get('bookticket/ticketprice/{trainno}', 'Onlineticket@ticketprice');
Route::post('bookticket/ticketprice/{trainno}/seatplan', 'Onlineticket@seatplan');
Route::get('seatlist', 'Onlineticket@seatlist');
Route::post('data/{sno}/{rnam}/{date}', 'Onlineticket@data');
Route::get('print/{unam}/{sno}', 'Onlineticket@prints');
Route::get('showticketprice', 'Onlineticket@showticketprice');
Route::get('showschedule', 'Onlineticket@showschedule');
Route::get('delseat', 'Onlineticket@delseat');
Route::get('logout', 'Onlineticket@logout');
